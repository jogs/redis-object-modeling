const GenericListType = require('./GenericListType')

module.exports = class QueueType extends GenericListType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
  }
  pop () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.rpop(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
