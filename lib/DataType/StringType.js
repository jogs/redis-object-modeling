const GenericType = require('./GenericType')

module.exports = class StringType extends GenericType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
  }
  get () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.get(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  set (data) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.setnx(self.fullNamespace, String(data), (err, reply) => {
        if (err) reject(err)
        else resolve(reply > 0)
      })
    })
  }
  update (data) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.set(self.fullNamespace, String(data), (err, reply) => {
        if (err) reject(err)
        else resolve(reply === 'OK')
      })
    })
  }
  length () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.strlen(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
