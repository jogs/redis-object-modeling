const StringType = require('./StringType')

module.exports = class NumberType extends StringType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
  }
  get () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.get(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(Number(reply))
      })
    })
  }
  increment (step = 1) {
    let self = this
    return new Promise(function (resolve, reject) {
      if (step % 1 === 0) {
        self.redis.incrby(self.fullNamespace, step, (err, reply) => {
          if (err) reject(err)
          else resolve(Number(reply))
        })
      } else {
        self.redis.incrbyfloat(self.fullNamespace, step, (err, reply) => {
          if (err) reject(err)
          else resolve(Number(reply))
        })
      }
    })
  }
  decrement (step = 1) {
    return this.increment(step * -1)
  }
}
