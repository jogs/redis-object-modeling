module.exports = class GenericType {
  constructor (namespace, name, client) {
    this.redis = client
    this.fullNamespace = `${namespace}:${name}`
  }
  get () {
    return null
  }
  set (data) {
    return null
  }
  update (data) {
    return null
  }
  remove () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.del(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(Boolean(reply))
      })
    })
  }
  expireIn (milliseconds) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.pexpire(self.fullNamespace, milliseconds, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  expireAt (timestamp) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.pexpireat(self.fullNamespace, timestamp, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
