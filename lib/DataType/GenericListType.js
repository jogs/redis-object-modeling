const GenericType = require('./GenericType')

module.exports = class List extends GenericType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
  }
  length () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.llen(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  get (start = 0, end) {
    let self = this
    return new Promise(function (resolve, reject) {
      if (!end) {
        self.redis.llen(self.fullNamespace, (err, length) => {
          if (err) reject(err)
          if (length) {
            end = length - 1
          } else resolve(null)
        })
      }
      self.redis.lrange(self.fullNamespace, start, end, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  set (value, multiple = false) {
    let self = this
    return new Promise(function (resolve, reject) {
      let query = []
      if (multiple) {
        query = [self.fullNamespace, ...value]
      } else {
        query = [self.fullNamespace, value]
      }
      self.redis.lpush(query, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  push (value) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.lpushx(self.fullNamespace, value, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  inversePush (value) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.rpushx(self.fullNamespace, value, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
