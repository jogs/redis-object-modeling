const StringType = require('./StringType')
const Bitwise = require('simple-bitwise')

module.exports = class BitmapType extends StringType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
    this.byte = new Bitwise()
    let ops = {
      return_buffers: true
    }
    if (client.auth_pass) ops.password = client.auth_pass
    if (client.selected_db) ops.db = client.selected_db
    this.bufferRedis = require('redis').createClient(Object.assign({},
      client.options, ops))
  }
  get () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.bufferRedis.get(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else {
          if (reply) {
            let bytes = reply.toJSON().data.map((current) => self.byte.toBitString(current))
            let result = ''
            bytes.map((byte) => result += byte)
            resolve(result)
          } else resolve(0)
        }
      })
    })
  }
  getBit (position = 0) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.getbit(self.fullNamespace, position, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  getPosition (bit = 1, start = null, end = null) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.bitpos([self.fullNamespace, bit, start, end], (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  setBit (offset, bit = 1) {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.setbit(self.fullNamespace, offset, bit, (err, reply) => {
        if (err) reject(err)
        else resolve(reply === 0)
      })
    })
  }
  count () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.bitcount(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
