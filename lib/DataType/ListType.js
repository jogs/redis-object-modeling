const GenericListType = require('./GenericListType')

module.exports = class ListType extends GenericListType {
  constructor (namespace, name, client) {
    super(namespace, name, client)
  }
  pop () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.rpop(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
  inversePop () {
    let self = this
    return new Promise(function (resolve, reject) {
      self.redis.lpop(self.fullNamespace, (err, reply) => {
        if (err) reject(err)
        else resolve(reply)
      })
    })
  }
}
