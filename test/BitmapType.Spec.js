const client = require('redis').createClient()
const BitmapType = require('../lib/DataType/BitmapType')

let bitmap = new BitmapType('bitmap', 'test', client)

describe('Type: Bitmap', () => {
  describe('CRUD', () => {
    it('Deberia regresar 0 al buscar en este namespace', (done) => {
      expect(bitmap.get())
      .to.eventually.be.equal(0).notify(done)
    })
    it('Deberia regresar 0 al contar los bit activos de este namespace', (done) => {
      expect(bitmap.count())
      .to.eventually.be.equal(0).notify(done)
    })
    it('Deberia definir el bitmap "1" y regresar true', (done) => {
      expect(bitmap.setBit(0))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 10000000 al buscar en este namespace', (done) => {
      expect(bitmap.get())
      .to.eventually.be.equal('10000000').notify(done)
    })
    it('Deberia regresar 1 al contar los bit activos de este namespace', (done) => {
      expect(bitmap.count())
      .to.eventually.be.equal(1).notify(done)
    })
    it('Deberia definir el bitmap 5 y regresar true', (done) => {
      expect(bitmap.setBit(5))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 10001000 al buscar en este namespace', (done) => {
      expect(bitmap.get())
      .to.eventually.be.equal('10000100').notify(done)
    })
    it('Deberia definir el bitmap 8 y regresar true', (done) => {
      expect(bitmap.setBit(8))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 1000010010000000 al buscar en este namespace', (done) => {
      expect(bitmap.get())
      .to.eventually.be.equal('1000010010000000').notify(done)
    })
    it('Deberia definir el bitmap 15 y regresar true', (done) => {
      expect(bitmap.setBit(15))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 1000010010000001 al buscar en este namespace', (done) => {
      expect(bitmap.get())
      .to.eventually.be.equal('1000010010000001').notify(done)
    })
    it('Deberia regresar 1 al buscar en el bit 0 de este namespace', (done) => {
      expect(bitmap.getBit(0))
      .to.eventually.be.equal(1).notify(done)
    })
    it('Deberia regresar 0 al buscar en el bit 14 de este namespace', (done) => {
      expect(bitmap.getBit(14))
      .to.eventually.be.equal(0).notify(done)
    })
    it('Deberia regresar 1 al buscar en el bit 15 de este namespace', (done) => {
      expect(bitmap.getBit(15))
      .to.eventually.be.equal(1).notify(done)
    })
    it('Deberia regresar 4 al contar los bit activos de este namespace', (done) => {
      expect(bitmap.count())
      .to.eventually.be.equal(4).notify(done)
    })
    it('Deberia regresar true al eliminar en este namespace', (done) => {
      expect(bitmap.remove())
      .to.eventually.be.equal(true).notify(done)
    })
  })
})
