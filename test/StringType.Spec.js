const client = require('redis').createClient()
const StringType = require('../lib/DataType/StringType')

let string = new StringType('string', 'test', client)

let str = new StringType('string', 'prueba', client)

describe('Type: String', () => {
  describe('CRUD', () => {
    it('Deberia regresar null al buscar en este namespace', (done) => {
      expect(string.get())
      .to.eventually.be.equal(null).notify(done)
    })
    it('Deberia definir el string "prueba" y regresar true', (done) => {
      expect(string.set('prueba'))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar "prueba" al buscar en este namespace', (done) => {
      expect(string.get())
      .to.eventually.be.equal('prueba').notify(done)
    })
    it('Deberia definir el string "prueba2" y regresar true', (done) => {
      expect(string.update('prueba2'))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar "prueba2" al buscar en este namespace', (done) => {
      expect(string.get())
      .to.eventually.be.equal('prueba2').notify(done)
    })
    it('Deberia regresar true al eliminar en este namespace', (done) => {
      expect(string.remove())
      .to.eventually.be.equal(true).notify(done)
    })
  })
})
