const client = require('redis').createClient()
const NumberType = require('../lib/DataType/NumberType')

let number = new NumberType('number', 'test', client)

describe('Type: Number', () => {
  describe('CRUD', () => {
    it('Deberia regresar 0 al buscar en este namespace', (done) => {
      expect(number.get())
      .to.eventually.be.equal(0).notify(done)
    })
    it('Deberia definir el number "0" y regresar true', (done) => {
      expect(number.set('0'))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 0 al buscar en este namespace', (done) => {
      expect(number.get())
      .to.eventually.be.equal(0).notify(done)
    })
    it('Deberia definir el number 5 y regresar true', (done) => {
      expect(number.update(5))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia regresar 5 al buscar en este namespace', (done) => {
      expect(number.get())
      .to.eventually.be.equal(5).notify(done)
    })
    it('Deberia regresar 6 al buscar en este namespace', (done) => {
      expect(number.increment())
      .to.eventually.be.equal(6).notify(done)
    })
    it('Deberia regresar 3 al buscar en este namespace', (done) => {
      expect(number.decrement(3))
      .to.eventually.be.equal(3).notify(done)
    })
    it('Deberia regresar 3.5 al buscar en este namespace', (done) => {
      expect(number.increment(0.5))
      .to.eventually.be.equal(3.5).notify(done)
    })
    it('Deberia regresar true al eliminar en este namespace', (done) => {
      expect(number.remove())
      .to.eventually.be.equal(true).notify(done)
    })
  })
})
