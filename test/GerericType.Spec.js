const client = require('redis').createClient()
const GenericType = require('../lib/DataType/GenericType')

let hola = new GenericType('hola', 'mundo', client)

// hola.keys().then((reply) => console.log(reply)).catch((err) => console.error(err))

describe('Type: Generic', () => {
  describe('Instancia', () => {
    it('Deberia tener una propiedad namespace', () => {
      expect(hola).to.have.property('namespace')
    })
    it('La propiedad namespace debe ser un string', () => {
      expect(hola.namespace).be.a('string')
    })
    it('Deberia tener una propiedad name', () => {
      expect(hola).to.have.property('name')
    })
    it('La propiedad name debe ser un string', () => {
      expect(hola.name).be.a('string')
    })
    it('Deberia tener una propiedad redis', () => {
      expect(hola).to.have.property('redis')
    })
    it('La propiedad redis debe ser un objeto', () => {
      expect(hola.redis).be.an('object')
    })
  })
})
